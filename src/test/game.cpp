#include <iostream>
#include <memory>
#include <stdexcept>

#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"

#include "../game.hpp"


namespace
{
void rollMany(Game & g, unsigned int _times, unsigned int _pins);
void rollSpare(Game & g);
void rollStrike(Game & g);
}

TEST_SUITE_BEGIN("GameTest");

TEST_CASE("setUpAndtearDown()")
{
    auto g = new Game();

    delete g;
}

TEST_CASE("gutterGame()")
{
    std::unique_ptr<Game> g(new Game());

    rollMany(*g, 20, 0);

    CHECK_EQ(0, g->score());
}

TEST_CASE("allOnes()")
{
    std::unique_ptr<Game> g(new Game());

    rollMany(*g, 20, 1);

    CHECK_EQ(20, g->score());
}

TEST_CASE("oneSpare()")
{
    std::unique_ptr<Game> g(new Game());

    rollSpare(*g);
    g->roll(3);
    rollMany(*g, 17, 0);

    CHECK_EQ(16, g->score());
}

TEST_CASE("oneStrike()")
{
    std::unique_ptr<Game> g(new Game());

    rollStrike(*g);
    g->roll(3);
    g->roll(4);
    rollMany(*g, 16, 0);

    CHECK_EQ(24, g->score());
}

TEST_CASE("perfectGame()")
{
    std::unique_ptr<Game> g(new Game());

    rollMany(*g, 12, 10);
    CHECK_EQ(300, g->score());
}

namespace
{
void rollMany(Game & g, unsigned int _times, unsigned int _pins)
{
    for(unsigned int i = 0; i < _times; ++i)
    {
        g.roll(_pins);
    }
}

void rollSpare(Game & g)
{
    g.roll(5);
    g.roll(5);
}

void rollStrike(Game & g)
{
    g.roll(10);
}
}

TEST_SUITE_END();
