binary_name			:= bowling
type				:= exec

include_paths			:= lib/doctest/install/include
library_paths			:=

source_paths			:= src
libs_to_link			:=

main_source_paths		:= src/main

test_source_paths		:= src/test
test_add_libs_to_link		:=

include lib/generic_makefile/modular_makefile.mk
